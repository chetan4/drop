/*global window*/
/*global $*/
/*global jQuery*/
/*global document*/
/*global swal*/

$(document).ready(function(){
  "use strict";
  $.fn.serializeObject = function (options) {
    options = jQuery.extend({}, options);

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
          "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
          "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
          "push": /^$/,
          "fixed": /^\d+$/,
          "named": /^[a-zA-Z0-9_]+$/
        };


    this.build = function (base, key, value) {
      base[key] = value;
      return base;
    };

    this.push_counter = function (key) {
      if (push_counters[key] === undefined) {
        push_counters[key] = 0;
      }
      return push_counters[key]++;
    };

    jQuery.each(jQuery(this).serializeArray(), function () {

      // skip invalid keys
      if (!patterns.validate.test(this.name)) {
        return;
      }

      var k,
          keys = this.name.match(patterns.key),
          merge = this.value,
          reverse_key = this.name;

      while ((k = keys.pop()) !== undefined) {

        // adjust reverse_key
        reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

        // push
        if (k.match(patterns.push)) {
          merge = self.build([], self.push_counter(reverse_key), merge);
        }

        // fixed
        else if (k.match(patterns.fixed)) {
          merge = self.build([], k, merge);
        }

        // named
        else if (k.match(patterns.named)) {
          merge = self.build({}, k, merge);
        }
      }

      json = jQuery.extend(true, json, merge);
    });


    return json;
  };

  var password = document.getElementById("password"),
			confirm_password = document.getElementById("confirm_password");

			function validateLength() {
				if (password.value.length > 5) {
					password.setCustomValidity("");
				} else {
					password.setCustomValidity("Password should contain minimum 6 characters");
				}
			}

			function validatePassword(){
				if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Passwords Don't Match");
			  } else {
			    confirm_password.setCustomValidity('');
			  }	
			}

			password.onchange = validatePassword;
			password.onkeyup = validateLength;
			confirm_password.onkeyup = validatePassword;
});
Drop = function () {};

Drop.Helper = function (config) {
    this.config = config;
    this.audio_tag = new Audio();
};

Drop.ChangePassword = function (rootScope) {
    this.helper = rootScope.helper;
};

Drop.Helper.prototype = {

};

Drop.prototype = {
  init: function (config) {
    var self = this;
    self.initEventListeners(config);

    switch (config.page_type){
      case "change_password":
        self.changePassword = new Drop.ChangePassword(self);
        self.changePassword.init();
        break;
    }

  },

  initEventListeners: function (config) {
    var self = this,
        is_home = config.page_type == "change_password";

    $(".change-passwordform").submit(function (e) {
      e.preventDefault();
      var jForm = $(this);
      function getUrlParameter(sParam) {
			    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			        sParameterName;

			        sParameterName = sPageURL.split('=');
			    jForm.find('.reset-token').val(sParameterName[1]);
			    console.log($('.reset-token').val());
			}
			getUrlParameter();

			var data = jForm.serializeObject();

			$.ajax({
	        url: 'http://54.83.143.201/doctors/password',
	        data: data,
	        contentType: "application/x-www-form-urlencoded",
	        method: "PUT",
	        dataType: "json",
	        beforeSend: function( xhr ) {
	        }
	    })
	    .done(function(response) {
	        if(response.success){
	          $('.change-passwordform').hide();
	          $('.thankyou-section').removeClass('hidden').show();
	        }
	    })
	    .fail(function(response) {
	      if(response.responseJSON.msg){
            swal({title: "Oops!",   text: response.responseJSON.msg,   type: "error",   confirmButtonText: "OK" });
        } 
	    });

    });
  }
};

Drop.ChangePassword.prototype = {
  init: function () {
    var self = this,
        helper = self.helper;
  }
};