var express = require('express'),
    is_production = (process.env.NODE_ENV === "prod"),
    key, routes,
    extend = require('util')._extend,
    router = express.Router(),
    util = require('./util'),
    global = require('./global_constant'),
    careers = global.careers,
    api_token = is_production ? '68cce3978e48c567836f8d248cf47a93' : 'f5fc3e38a7e2d9722e65aabc92aaaa5a',
    domain = is_production ? '//idyllic.co:8010/' : '//localhost:8001/';

routes = {
    home: {
        url: '/',
        asset_path: 'index.html',
        page_data: {
            page_type: 'home',
            menu_title: 'Drop'
        },
    },
    change_password: {
        url: '/change_password',
        asset_path: 'change_password.html',
        page_data: {
            page_type: 'change_password',
            menu_title: 'Change Password'
        },
    },
    details: {
        url: '/details',
        asset_path: 'details.html',
    },
    contact_us: {
        url: '/contact_us',
        asset_path: 'contact_us.html',
    },
    about_us: {
        url: '/about_us',
        asset_path: 'about_us.html',
    },
    refunds: {
        url: '/refunds',
        asset_path: 'refunds.html',
    },
    how_it_works: {
        url: '/how_it_works',
        asset_path: 'how_it_works.html',
    },
    privacy_policy: {
        url: '/privacy_policy',
        asset_path: 'privacy_policy.html',
    },
    terms: {
        url: '/terms',
        asset_path: 'terms.html',
    },
    coming_soon: {
        url: '/app_store',
        asset_path: 'coming_soon.html',
    },
};


var paths = {

};
for(key in routes){
    paths[key + '_path'] = routes[key].url;
}

for(key in routes){
    (function(route, name){
        var configs = {
            page: {
                name: name,
                body_cls: name + '_page',
                js_data: {
                    baseUrl: domain,
                    apiKey: api_token
                }
            },
            paths: paths,
            is_production: is_production,
            contact_us_tags: global.contact_us_tags
        };
        extend(configs.page, route.page_data);

        if(route.hasOwnProperty('global_data')) {
            extend(configs, route.global_data);
        }

        router.get(route.url, function(req, res) {
            if(route.hasOwnProperty('beforeLoad')){
                route.beforeLoad(configs);
            }
            res.render(route.asset_path, configs);
        });

    })(routes[key], key);
}

module.exports = router;
